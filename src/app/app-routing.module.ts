import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CrudWrapperComponent } from './components/crud-wrapper/crud-wrapper.component';
import { CrudifyModule } from '../../projects/tza/crudify/src/lib/crudify.module';

@NgModule({
  imports: [
    CrudifyModule,
    RouterModule.forRoot([
      {
        path: '',
        component: CrudWrapperComponent,
        pathMatch: 'full',
        data: {
          items: [
            { name: 'tata', id: 1 },
            { name: 'toto', id: 2 },
            { name: 'titi', id: 3 },
          ]
        }
      }
    ])
  ],
})
export class AppRoutingModule {

}
