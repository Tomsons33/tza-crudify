import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CrudifyModule } from '@tza/crudify';
import { CrudWrapperComponent } from './components/crud-wrapper/crud-wrapper.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { ListItemComponent } from './components/list-item/list-item.component';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CrudWrapperComponent,
    ListItemComponent,
    ItemFormComponent
  ],
  imports: [
    BrowserModule,
    CrudifyModule,
    AppRoutingModule,
    RouterModule,
    ReactiveFormsModule
  ],
  providers: [],
  entryComponents: [
    ListItemComponent,
    ItemFormComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
