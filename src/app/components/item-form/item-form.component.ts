import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Editable } from '@tza/crudify';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss']
})
export class ItemFormComponent implements OnChanges, Editable {

  private onEdited: (value: any) => void;

  public form = new FormGroup({
    id: new FormControl(null, []),
    name: new FormControl(null, [Validators.required])
  });

  public ngOnChanges(changes: SimpleChanges): void {
    this.form.patchValue(changes.data.currentValue);
  }

  public save() {
    this.onEdited(this.form.value);
    this.form.reset();
  }

  public registerOnEdit(fn: (value: any) => void) {
    this.onEdited = fn;
  }
}
