import { Component } from '@angular/core';
import { CrudComponentOptions } from '@tza/crudify';
import { ListItemComponent } from '../list-item/list-item.component';
import { ItemFormComponent } from '../item-form/item-form.component';

@Component({
  selector: 'app-crud-wrapper',
  templateUrl: './crud-wrapper.component.html',
  styleUrls: ['./crud-wrapper.component.scss']
})
export class CrudWrapperComponent {
  public options: CrudComponentOptions = {
    listItemComponent: ListItemComponent,
    formComponent: ItemFormComponent
  };
}
