import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudWrapperComponent } from './crud-wrapper.component';

describe('CrudWrapperComponent', () => {
  let component: CrudWrapperComponent;
  let fixture: ComponentFixture<CrudWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
