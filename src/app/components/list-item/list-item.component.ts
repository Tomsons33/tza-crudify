import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { Editable, Deletable, Matchable } from '@tza/crudify';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnChanges, Editable, Deletable, Matchable {

  public item: any;
  public onEdit: (value: any) => void;
  public onDelete: (value: any) => void;

  public ngOnChanges(changes: SimpleChanges): void {
    this.item = changes.data.currentValue;
  }

  public registerOnDelete(fn: (value: any) => void) {
    this.onDelete = fn;
  }

  public registerOnEdit(fn: (value: any) => void) {
    this.onEdit = fn;
  }

  public edit() {
    this.onEdit(this.item);
  }

  public delete() {
    this.onDelete(this.item);
  }

  public matches(value: any): boolean {
    return value.id === this.item.id;
  }
}
