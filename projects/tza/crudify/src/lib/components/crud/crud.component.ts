import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Input, OnChanges,
  OnInit,
  QueryList,
  SimpleChange,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { delay, pluck, tap } from 'rxjs/operators';
import { CrudComponentOptions } from '../../interfaces/crud-component-options';
import { Deletable } from '../../interfaces/deletable';
import { Editable } from '../../interfaces/editable';
import { Matchable } from '../../interfaces/matchable';

@Component({
  selector: 'lib-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrudComponent implements OnInit {

  @Input()
  public routeDataKey: string;
  @Input()
  public options: CrudComponentOptions;
  public items: any[];
  @ViewChildren('listItem', {read: ViewContainerRef})
  public itemList: QueryList<ViewContainerRef>;
  @ViewChild('formWrapper', {read: ViewContainerRef})
  public formWrapper: ViewContainerRef;
  private itemRefs: (OnChanges & Deletable & Editable & Matchable)[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly cfr: ComponentFactoryResolver,
    private readonly cdr: ChangeDetectorRef
  ) { }

  public ngOnInit() {
    this.route.data
      .pipe(
        pluck<Data, any[]>(this.routeDataKey),
        tap(items => this.items = items),
        delay(100)
      )
      .subscribe(() => this.setupInterface());
  }

  private setupInterface() {
    const factory = this.cfr.resolveComponentFactory(this.options.formComponent);
    const formRef = this.formWrapper.createComponent(factory);

    const itemWrapperArray = this.itemList.toArray();
    this.itemRefs = this.items.map((item, index) => {
      const itemFactory = this.cfr.resolveComponentFactory(this.options.listItemComponent);
      const ref = itemWrapperArray[index].createComponent(itemFactory);
      ref.instance.ngOnChanges({
        data: new SimpleChange(null, item, true)
      });
      ref.instance.registerOnDelete(_ => {
        this.items = this.items.filter(i => !ref.instance.matches(i));
      });
      ref.instance.registerOnEdit(value => {
        formRef.instance.ngOnChanges({
          data: new SimpleChange(null, value, true)
        });
      });
      this.cdr.markForCheck();
      return ref.instance;
    });

    formRef.instance.registerOnEdit(value => {
      this.itemRefs.filter(ref => ref.matches(value))
        .map(ref => {
          ref.ngOnChanges({
            data: new SimpleChange(null, value, false)
          });
        });
    });
  }
}
