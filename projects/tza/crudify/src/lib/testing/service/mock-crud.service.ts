import { CrudService } from '../../service/crud.service';
import { ModelMock } from '../model/model-mock';
import { ModelMockRepository } from '../repository/model-mock.repository';

export class MockCrudService extends CrudService<ModelMock> {
  constructor() {
    super(new ModelMockRepository(), '');
  }
}
