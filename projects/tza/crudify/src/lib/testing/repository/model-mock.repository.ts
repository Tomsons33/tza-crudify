import { MemoryCrudRepository } from '../../repository/memory-crud.repository';
import { ModelMock } from '../model/model-mock';

export class ModelMockRepository extends MemoryCrudRepository<ModelMock> {

}
