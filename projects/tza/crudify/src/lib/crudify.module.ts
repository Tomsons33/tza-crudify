import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrudComponent } from './components/crud/crud.component';

@NgModule({
  declarations: [CrudComponent],
  imports: [
    CommonModule
  ],
  exports: [CrudComponent]
})
export class CrudifyModule { }
