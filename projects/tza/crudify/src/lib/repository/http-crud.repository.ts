import {ICrudRepositoryInterface} from './crud.repository.interface';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

export abstract class HttpCrudRepository<T> implements ICrudRepositoryInterface<T> {
  constructor(
    protected readonly http: HttpClient,
    protected readonly resourcePath: string
  ) {}

  public all(options?: {}): Observable<T[]> {
    return this.http.get<T[]>(this.resourcePath, options);
  }

  public create(item: T | FormData, options?: {}): Observable<T> {
    return this.http.post<T>(this.resourcePath, item, options);
  }

  public delete(id: string | number, options?: {}): Observable<void> {
    return this.http.delete<void>(`${this.resourcePath}/${id}`, options);
  }

  public get(id: string | number, options?: {}): Observable<T> {
    return this.http.get<T>(`${this.resourcePath}/${id}`, options);
  }

  public update(id: string | number, item: T | FormData, options?: {}): Observable<T> {
    return this.http.put<T>(`${this.resourcePath}/${id}`, item, options);
  }
}
