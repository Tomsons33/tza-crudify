import {Observable} from 'rxjs';

export interface ICrudRepositoryInterface<T> {
  all(options?: any): Observable<T[]>;
  get(id: string | number, options?: any): Observable<T>;
  create(item: T | FormData, options?: any): Observable<T>;
  update(id: string | number, item: T | FormData, options?: any): Observable<T>;
  delete(id: string | number, options?: any): Observable<void>;
}
