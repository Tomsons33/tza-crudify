import { ICrudRepositoryInterface } from './crud.repository.interface';
import { EMPTY, Observable, of, throwError } from 'rxjs';

export class MemoryCrudRepository<T> implements ICrudRepositoryInterface<T> {
  constructor(private readonly items: T[] = []) {}

  public all(options?: any): Observable<T[]> {
    return of(this.items);
  }

  public create(item: T, options?: any): Observable<T> {
    this.items.push(item);
    return of(item);
  }

  public delete(id: string | number, options?: any): Observable<void> {
    return EMPTY;
  }

  public get(index: number, options?: any): Observable<T> {
    return index < this.items.length ? of(this.items[index]) : throwError(`${index} out of bounds`);
  }

  public update(index: number, item: T, options?: any): Observable<T> {
    this.items[index] = item;
    return of(item);
  }
}
