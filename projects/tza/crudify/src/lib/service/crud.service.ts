import {ICrudRepositoryInterface} from '../repository/crud.repository.interface';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export abstract class CrudService<T> implements Resolve<T | T[]> {
  constructor(
    protected readonly repository: ICrudRepositoryInterface<T>,
    protected readonly idSelector: string
  ) {}

  public resolve(route?: ActivatedRouteSnapshot, state?: RouterStateSnapshot): Observable<T[] | T> {
    return route && route.paramMap.has(this.idSelector) ? this.get(route.paramMap.get(this.idSelector)) : this.getAll();
  }

  public getAll(): Observable<T[]> {
    return this.repository.all();
  }

  public get(id: string | number): Observable<T> {
    return this.repository.get(id);
  }

  public save(item: T | FormData): Observable<T> {
    const id = item instanceof FormData ? this.extractId(item) : item[this.idSelector];
    return id ? this.repository.update(id, item) : this.repository.create(item);
  }

  public delete(id: string | number): Observable<void> {
    return this.repository.delete(id);
  }

  protected extractId(item: FormData): string {
    return item.has(this.idSelector) ? item.get(this.idSelector).toString() : null;
  }
}
