import { MockCrudService } from '../testing/service/mock-crud.service';
import { fakeAsync, tick } from '@angular/core/testing';
import { ModelMock } from '../testing/model/model-mock';

describe('CrudService', () => {

  let service: MockCrudService;

  beforeEach(() => {
    service = new MockCrudService();
  });

  it('should return an empty list of items',  fakeAsync(() => {
    let items: ModelMock[];
    service.getAll().subscribe(res => items = res);
    tick();
    expect(items.length).toBe(0);
  }));
});
