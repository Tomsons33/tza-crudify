export interface Matchable {
  matches(value: any): boolean;
}
