export interface Editable {
  registerOnEdit(fn: (value: any) => void);
}
