export interface Deletable {
  registerOnDelete(fn: (value: any) => void);
}
