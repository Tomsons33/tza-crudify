import { OnChanges, Type } from '@angular/core';
import { Deletable } from './deletable';
import { Editable } from './editable';
import { Matchable } from './matchable';

export interface CrudComponentOptions {
  listItemComponent: Type<OnChanges & Deletable & Editable & Matchable>;
  formComponent: Type<OnChanges & Editable>;
}
