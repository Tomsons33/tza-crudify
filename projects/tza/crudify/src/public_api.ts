/*
 * Public API Surface of crudify
 */

export * from './lib/service/crud.service';
export * from './lib/repository/http-crud.repository';
export * from './lib/repository/crud.repository.interface';
export * from './lib/crudify.module';
export * from './lib/interfaces/crud-component-options';
export * from './lib/interfaces/deletable';
export * from './lib/interfaces/editable';
export * from './lib/interfaces/matchable';
